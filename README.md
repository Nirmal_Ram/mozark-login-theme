# Mozark Login Theme Using Keycloakify

This repo constitutes an easily reusable CI setup for SPA React App in general, and Apps that generates Keycloaks's theme 
using [keycloakify](https://github.com/InseeFrLab/keycloakify) in particular.

Follow the below procedure to run and execute mozark theme.

step 1 : clone this repository to your local machine 


```shell
git clone https://Nirmal_Ram@bitbucket.org/Nirmal_Ram/mozark-login-theme.git
```

step 2 : install all dependencies 


```shell
yarn
```

step 3 : build the theme for keycloak using following commands 


```shell
yarn keycloak
```

step 4 : start your keycloak docker container using following command 


```shell
yarn start
```

##### Go to your keycloak admin panel : http://localhost:8080/

### Make change and build 

once you done changes in ../src/styles/keycloak.css you have to do the following to reflect changes


step 1 : make sure to delete ./build and ./build_keycloak folder from your project folder

step 2 : make sure to stop your running container and delete all images associated with project

```shell
 docker stop $(docker ps -aq)
 docker rm $(docker ps -aq)
 docker rmi $(docker images -aq)
```

step 3 : build the theme for keycloak using following commands


```shell
yarn keycloak
```

step 4 : start your keycloak docker container using following command


```shell
yarn start
```

### Go to your keycloak admin panel : http://localhost:8080/